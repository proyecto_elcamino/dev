# !/usr/bin/python3

from mazelib import Maze
from mazelib.generate.Prims import Prims
m = Maze()
m.generator = Prims(10, 10)
m.generate()


def inicio_juego():
    print("\t       UN PERRITO OLVIDÓ DONDE ENTERRÓ SU HUESO")
    print("\t                      🐕🍖❓")
    print("\tEl perrito te pide que lo ayudes a encontrar su huesito \n")


inicio_juego()
print(m)
